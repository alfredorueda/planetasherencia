package com.example.library;

import com.example.library.Model.*;
import com.example.library.Repositories.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.ArrayList;
import java.util.Date;

@SpringBootApplication
@EnableJpaRepositories
public class SpringDataExampleLibraryApplication {

    public static void main(String[] args) {
	    ConfigurableApplicationContext context = SpringApplication.run(SpringDataExampleLibraryApplication.class, args);

        LunaRepository lunaRepository = context.getBean(LunaRepository.class);
        PlanetRepository planetaRepository = context.getBean(PlanetRepository.class);
        ManeuverRepository maneuverRepository=context.getBean(ManeuverRepository.class);

        Luna luna1=new Luna();
        Luna luna2=new Luna();
        Luna luna3=new Luna();
        luna1.setInclination(12);
        luna1.setName("Titan");
        luna2.setInclination(21);
        luna2.setName("Callisto");
        luna3.setInclination(3);
        luna3.setName("Hyperion");

        lunaRepository.save(luna1);
        lunaRepository.save(luna2);
        lunaRepository.save(luna3);

        Planeta planeta1=new Planeta();
        Planeta planeta2=new Planeta();
        Planeta planeta3=new Planeta();
        planeta1.setName("Tierra");
        planeta1.setRingCount(0);
        planeta2.setName("Saturno");
        planeta2.setRingCount(3);
        planeta3.setName("Jupiter");
        planeta3.setRingCount(5);

        planetaRepository.save(planeta1);
        planetaRepository.save(planeta2);
        planetaRepository.save(planeta3);

        Maneuver m1=new Maneuver();
        Maneuver m2=new Maneuver();
        Maneuver m3=new Maneuver();

        m1.setName("Maniobra1");
        m1.setCuerpoOrigen(planeta2);
        m1.setCuerpoDestino(luna3);

        m2.setName("Maniobra2");
        m2.setCuerpoOrigen(planeta2);
        m2.setCuerpoDestino(planeta1);

        m3.setName("Maniobra3");
        m3.setCuerpoOrigen(luna3);
        m3.setCuerpoDestino(luna3);

        maneuverRepository.save(m1);
        maneuverRepository.save(m2);
        maneuverRepository.save(m3);

        System.exit(0);
    }
}
