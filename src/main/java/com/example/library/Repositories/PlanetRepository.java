package com.example.library.Repositories;

        import com.example.library.Model.Planeta;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface PlanetRepository extends PagingAndSortingRepository<Planeta, Long> {
}
