package com.example.library.Repositories;

import com.example.library.Model.Maneuver;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface ManeuverRepository extends PagingAndSortingRepository<Maneuver, Long> {
}