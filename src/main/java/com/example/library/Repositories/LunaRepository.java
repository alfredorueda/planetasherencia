package com.example.library.Repositories;

import com.example.library.Model.Luna;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface LunaRepository extends PagingAndSortingRepository<Luna, Long> {
}
