package com.example.library.Model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="id")
public class Planeta extends CuerpoCeleste{

    private int ringCount;

    public int getRingCount() {
        return ringCount;
    }

    public void setRingCount(int ringCount) {
        this.ringCount = ringCount;
    }

	public Planeta() {
	}

    @Override
    public String toString() {
        return "Planeta{" +
                "ringCount=" + ringCount +
                '}';
    }
}
