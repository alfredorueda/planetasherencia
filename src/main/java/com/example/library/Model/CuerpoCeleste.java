package com.example.library.Model;

import javax.persistence.*;


@Entity
@Inheritance(strategy= InheritanceType.JOINED)
public class CuerpoCeleste {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	protected Long id;

	@Column
	protected String name;

	public CuerpoCeleste() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CuerpoCeleste{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}
}
