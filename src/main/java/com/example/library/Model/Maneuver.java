package com.example.library.Model;

import javax.persistence.*;


@Entity
public class Maneuver {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    protected Long id;

    private String name;

    @ManyToOne
    private CuerpoCeleste cuerpoOrigen;

	@ManyToOne
    private CuerpoCeleste cuerpoDestino;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CuerpoCeleste getCuerpoOrigen() {
        return cuerpoOrigen;
    }

    public void setCuerpoOrigen(CuerpoCeleste cuerpoOrigen) {
        this.cuerpoOrigen = cuerpoOrigen;
    }

    public CuerpoCeleste getCuerpoDestino() {
        return cuerpoDestino;
    }

    public void setCuerpoDestino(CuerpoCeleste cuerpoDestino) {
        this.cuerpoDestino = cuerpoDestino;
    }

    @Override
    public String toString() {
        return "Maneuver{" +
                "name='" + name + '\'' +
                ", cuerpoOrigen=" + cuerpoOrigen +
                ", cuerpoDestino=" + cuerpoDestino +
                '}';
    }
}
