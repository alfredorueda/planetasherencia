package com.example.library.Model;

import javax.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name="id")
public class Luna extends CuerpoCeleste{

    private int inclination;

    public int getInclination() {
        return inclination;
    }

    public void setInclination(int inclination) {
        this.inclination = inclination;
    }

    @Override
    public String toString() {
        return "Luna{" +
                "inclination=" + inclination +
                '}';
    }

	public Luna() {
	}


}
